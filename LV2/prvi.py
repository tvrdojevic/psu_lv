import numpy as np
import matplotlib.pyplot as plt

xValue = (1, 2, 3, 3, 1)
yValue = (1, 2, 2, 1, 1)
plt.xlim(0, 4)
plt.ylim(0, 4)
plt.plot(xValue, yValue)
plt.show()