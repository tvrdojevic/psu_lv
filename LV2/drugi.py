import numpy as np
import matplotlib.pyplot as plt

rollResults = dict()
rolls = []
for i in range(101):
    diceSide = np.random.randint(1, 7)
    rolls.append(diceSide)
    if diceSide not in rollResults:
        rollResults[diceSide] = 1
    else:
        rollResults[diceSide] += 1

for i in range(1, 7):
    print(rollResults.get(i))

plt.hist(rolls, bins = 6)
plt.show()