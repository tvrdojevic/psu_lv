import matplotlib.pyplot as plt
import numpy as np
import skimage.io

img = skimage.io.imread("LV2\\tiger.png", as_gray=True)
rows = int(img.size / img[0].size)
cols = img[0].size


"""
rotatedImg = np.zeros((cols, rows))
for i in range(rows):
    for j in range(cols):
        rotatedImg[cols - 1 - j][i] = img[i][j]
img = rotatedImg

plt.imshow(img, cmap='gray', vmin=0, vmax=255)
plt.show()
"""

"""
for i in range(rows):
    for j in range(cols):
        img[i][j] *= -1

plt.imshow(img, cmap='gray', vmin=0, vmax=255)
plt.show()
"""

"""
mirroredImg = np.zeros((rows, cols))
for i in range(rows):
    for j in range(cols):
        mirroredImg[i][j] = img[i][cols - 1 - j]
img = mirroredImg
"""

"""
lowerResRows = int(rows/10)
lowerResCols = int(cols/10)
lowerResImg = np.zeros((lowerResRows, lowerResCols))
for i in range(lowerResRows):
    for j in range(lowerResCols):
        lowerResImg[i][j] = img[i*10][(lowerResCols - 1 - j)*10]
img = lowerResImg
"""

"""
for i in range(rows):
        for j in range(cols):
            if(j < int(rows/4) or j > int(rows/2)):
                img[i][j] = 0
            

plt.imshow(img, cmap='gray', vmin=0, vmax=255)
plt.show()
"""
