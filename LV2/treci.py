import numpy as np
import matplotlib.pyplot as plt

data = np.loadtxt(open("LV2\mtcars.csv", "rb"), usecols=(1,2,3,4,5,6),
delimiter=",", skiprows=1)

xValue = []
yValue = []
weight = []

minMpg = data[0][0]
maxMpg = data[0][0]
sumMpg = 0
count = 0
for value in data:
    xValue.append(value[0]) 
    yValue.append(value[3])
    weight.append(value[5] * 10)
    sumMpg += value[0]
    count += 1
    if(value[0] > maxMpg):
        maxMpg = value[0]
    if(value[0] < minMpg):
        minMpg = value[0]

print("srednja vrijednost", sumMpg / count, " min: ", minMpg, " max: ", maxMpg)

plt.scatter(xValue, yValue, weight)
plt.show()

xValue6c = []
yValue6c = []
weight6c = []

minMpg6c = data[0][0]
maxMpg6c = data[0][0]
sumMpg6c = 0
count6c = 0
for value in data:
    if(value[1] == 6):
        xValue6c.append(value[0]) 
        yValue6c.append(value[3])
        weight6c.append(value[5] * 10)
        sumMpg6c += value[0]
        count6c += 1
        if(value[0] > maxMpg6c):
            maxMpg6c = value[0]
        if(value[0] < minMpg6c):
            minMpg6c = value[0]

print("srednja vrijednost", sumMpg6c / count6c, " min: ", minMpg6c, " max: ", maxMpg6c)

plt.scatter(xValue6c, yValue6c, weight6c)
plt.show()
